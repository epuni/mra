package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {
	
	private static List<String> list(String... args)
	{
		return Arrays.asList(args);
	}
	
	@Test
	public void testEmptyInitialization() throws MarsRoverException {
		new MarsRover(10, 10, null);
	}
	
	@Test(expected = MarsRoverException.class)
	public void testInvalidSize() throws MarsRoverException {
		new MarsRover(10, -10, null);
	}
	
	@Test
	public void testInitialization() throws MarsRoverException {
		var rover = new MarsRover(10, 10, list("(1,2)", "(2,4)"));
		assertTrue(rover.planetContainsObstacleAt(1, 2));
		assertFalse(rover.planetContainsObstacleAt(1, 1));
	}
	
	@Test(expected = MarsRoverException.class)
	public void testMalformedString() throws MarsRoverException {
		new MarsRover(10, 10, list("(1,a)","(2,1)"));
	}
	
	@Test(expected = MarsRoverException.class)
	public void testMalformedString2() throws MarsRoverException {
		new MarsRover(10, 10, list("(1,)","(2,1)"));
	}
	
	@Test(expected = MarsRoverException.class)
	public void testMalformedString3() throws MarsRoverException {
		new MarsRover(10, 10, list("(1,2)","(2,1"));
	}
	
	@Test
	public void testInitialPosition() throws MarsRoverException {
		var rover = new MarsRover(10, 10, null);
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testChangeDirection() throws MarsRoverException {
		var rover = new MarsRover(10, 10, null);
		assertEquals("(0,0,E)", rover.executeCommand("r"));
		
		rover = new MarsRover(10, 10, null);
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testMoving() throws MarsRoverException {
		var rover = new MarsRover(10, 10, null);
		assertEquals("(0,1,N)", rover.executeCommand("f"));
		
		assertEquals("(0,1,E)", rover.executeCommand("r"));
		
		assertEquals("(1,1,E)", rover.executeCommand("f"));
	}
	
	@Test
	public void testMoveBackwards() throws MarsRoverException {
		var rover = new MarsRover(10, 10, null);
		assertEquals("(0,1,N)", rover.executeCommand("f"));
		assertEquals("(0,0,N)", rover.executeCommand("b"));		
	}
	
	@Test
	public void testMultipleCommands() throws MarsRoverException {
		var rover = new MarsRover(10, 10, null);
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}
	
	@Test
	public void testWrapping() throws MarsRoverException {
		var rover = new MarsRover(10, 10, null);
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void testSingleObstacle() throws MarsRoverException {
		var rover = new MarsRover(10, 10, list("(2,2)"));
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
	}
	
	@Test
	public void testMultipleObstacles() throws MarsRoverException {
		var rover = new MarsRover(10, 10, list("(2,2)", "(2,1)"));
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
	}
	
	@Test
	public void testObstacleOnWrap() throws MarsRoverException {
		var rover = new MarsRover(10, 10, list("(0,9)"));
		assertEquals("(0,0,N)(0,9)", rover.executeCommand("b"));
	}
	
	@Test
	public void testPlanetTour() throws MarsRoverException {
		var rover = new MarsRover(6, 6, list("(2,2)", "(0,5)", "(5,0)"));
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
}
