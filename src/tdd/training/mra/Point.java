package tdd.training.mra;

import java.text.ParseException;
import java.util.Objects;
import java.util.regex.Pattern;

public class Point {
	private static final Pattern coordMatcher = Pattern.compile("^\\((\\d),(\\d)\\)$");
	
	public final int x;
	public final int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Point add(Point other) {
		return new Point(x + other.x, y + other.y);
	}

	public static Point parse(String s) throws ParseException
	{
		var m = coordMatcher.matcher(s);
		if (!m.find())
			throw new ParseException("\"" + s + "\" couldn't be parsed as valid coordinates", 0);
		
		var x = Integer.parseInt(m.group(1));
		var y = Integer.parseInt(m.group(2));	
		
		return new Point(x, y);
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	
	// Auto generated
	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Point))
			return false;
		Point other = (Point) obj;
		return x == other.x && y == other.y;
	}
}
