package tdd.training.mra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class MarsRover {

	final HashSet<Point> obstacles = new HashSet<>();
	final Point worldSize;
	
	Point position = new Point(0, 0);
	Direction direction = Direction.NORTH;
	
	private static final HashMap<Direction, Point> dirToOffset = new HashMap<>() {{
		put(Direction.NORTH, new Point(0,1));
		put(Direction.EAST, new Point(1,0));
		put(Direction.SOUTH, new Point(0,-1));
		put(Direction.WEST, new Point(-1,0));
    }};
    
    private static final char CMD_RIGHT = 'r';
    private static final char CMD_LEFT = 'l';
    private static final char CMD_FORWARD = 'f';
    private static final char CMD_BACKWARD = 'b';
    
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if (planetX < 0 || planetY < 0)
			throw new MarsRoverException("Invalid planet size");
		
		worldSize = new Point(planetX, planetY);
		
		if (planetObstacles != null)
		{
			for (var s : planetObstacles)
			{
				try {
					obstacles.add(Point.parse(s));	
				} 
				catch (Exception ex) {
					throw new MarsRoverException(ex.getMessage());
				}
			}
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {
		return obstacles.contains(new Point(x,y));
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		List<Point> cmdObstacles = new ArrayList<>();
		
		for (var c : commandString.toCharArray())
		{
			if (c == CMD_RIGHT)
				direction = direction.right();
			else if (c == CMD_LEFT)
				direction = direction.left();
			else if (c == CMD_FORWARD)
				move(direction, cmdObstacles);
			else if (c == CMD_BACKWARD)
				move(direction.opposite(), cmdObstacles);
			else throw new MarsRoverException("Unknown command");
		}
		
		return serializeStatus() + 
			String.join("", cmdObstacles.stream()
				.map(Point::toString)
				.toArray(String[]::new));
	}
	
	private boolean move(Direction d, List<Point> obstacles)
	{
		var npos = normalizePosition(position.add(dirToOffset.get(d)));
		
		if (planetContainsObstacleAt(npos.x, npos.y))
		{
			if (!obstacles.contains(npos))
				obstacles.add(npos);
			
			return false;
		}
		
		position = npos;
		return true;
	}
	
	private Point normalizePosition(Point p)
	{
		var x = p.x;
		var y = p.y;
		
		while (x < 0) x = worldSize.x + x;
		while (y < 0) y = worldSize.y + y;
		
		if (x >= worldSize.x) x = x % worldSize.x;
		if (y >= worldSize.x) y = y % worldSize.y;
		
		return new Point(x,y);
	}
	
	private String serializeStatus() {
		return "(" + position.x + "," + position.y + "," + direction + ")";
	}

}
