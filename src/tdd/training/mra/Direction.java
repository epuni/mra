package tdd.training.mra;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Direction {
	public static final Direction NORTH = new Direction('N');
	public static final Direction WEST = new Direction('W');
	public static final Direction EAST = new Direction('E');
	public static final Direction SOUTH = new Direction('S');
	
	private char value;
	
	private static final List<Direction> order = Arrays.asList(
			NORTH, EAST, SOUTH, WEST
	);
	
	public Direction opposite() {
		var i = order.indexOf(this);
		return order.get((i + 2) % order.size()); 
	}
	
	public Direction left() {
		var i = order.indexOf(this);
		if (i == 0)
			return order.get(order.size() - 1);
		return order.get(i - 1); 
	}
	
	public Direction right() {
		var i = order.indexOf(this);
		if (i == order.size() - 1)
			return order.get(0);
		return order.get(i + 1);
	}
	
	private Direction(char value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Direction))
			return false;
		Direction other = (Direction) obj;
		return value == other.value;
	}

	@Override
	public String toString() {
		return "" + value;
	}
}
